#include "libft.h"

void ft_remove_char(char *str, char to_remove)
{
    unsigned int index;

    index = 0;
    while (str[index])
    {
        if (str[index] == to_remove)
        {
            ft_memmove(&str[index], &str[index + 1], ft_strlen(str) - index);
            index--;
        }
        index++;
    }
}